package melby.poc.springboot.application;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.context.annotation.Import;
import melby.poc.springboot.config.JavaSecurityConfig;
import melby.poc.springboot.config.TestJavaSecurityConfig;
import melby.poc.springboot.config.WebConfig;

@SpringBootApplication
@Import({ // @formatter:off
		WebConfig.class,TestJavaSecurityConfig.class }) // @formatter:on
public class ProductsApp {

	public ProductsApp() {
		super();
	}

	public static void main(final String... args) {
		SpringApplication.run(ProductsApp.class, args);
	}

}
