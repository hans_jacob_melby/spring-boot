package melby.poc.springboot.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class TestJavaSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication().withUser("hjm").password("hjm").roles("USER").and().withUser("admin")
				.password("admin").roles("ADMIN");

	}

	@Override
	protected void configure(final HttpSecurity http) throws Exception {
		// @formatter:off
		// http.
		// httpBasic().
		// and().
		// authorizeRequests().
		// //antMatchers(HttpMethod.GET,"/products/**").permitAll().
		// //antMatchers(HttpMethod.POST,"/products/**").authenticated().
		// antMatchers("/index.html", "/home.html", "/login.html",
		// "/").permitAll().
		// anyRequest().authenticated().
		// and().
		// sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().
		// csrf().disable();
		// @formatter:on

		http.httpBasic().and().authorizeRequests().antMatchers("/index.html", "/home.html", "/login.html", "/logout")
				.permitAll().anyRequest().authenticated().and().csrf().and()
				.logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/index.html");
	}
}
