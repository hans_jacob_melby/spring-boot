package melby.poc.springboot.web.controllers;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
@RestController
@RequestMapping("/neo")
public class NeoController {
	private static final Logger logger = LoggerFactory.getLogger(NeoController.class);
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<Void> test() {
		logger.debug("Trying to test neo connection");
		
		
		
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
}
