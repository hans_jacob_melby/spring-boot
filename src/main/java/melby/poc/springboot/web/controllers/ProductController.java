package melby.poc.springboot.web.controllers;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.UUID;


import org.apache.commons.lang.NotImplementedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import melby.poc.springboot.web.domain.Product;
import melby.poc.springboot.web.domainv2.EnviromentInfo;
import melby.poc.springboot.web.domainv2.ProductV2;

@RestController
@RequestMapping("/products")
public class ProductController {
	private static final Logger logger = LoggerFactory.getLogger(ProductController.class);
	private static Hashtable<UUID, Product> storage = new Hashtable<UUID, Product>();

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Product> create(@RequestBody final Product product) {
		System.out.println("inside create");
		RestPreconditions.validate(product);
		Product storedProduct = new Product(UUID.randomUUID(), product.getProductName(), product.getPrice());
		storedProduct.add(
				linkTo(methodOn(ProductController.class).get(storedProduct.getProductID().toString())).withSelfRel());
		storage.put(storedProduct.getProductID(), storedProduct);
		return new ResponseEntity<Product>(storedProduct, HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "{id}")
	public void delete(@PathVariable String id) {
		throw new NotImplementedException("Not implemented yet");
	}

	@RequestMapping(value = "/{productID}")
	public ResponseEntity<Product> get(@PathVariable String productID) {
		logger.debug("Trying to find product with id " + productID);

		if (storage.containsKey(UUID.fromString(productID))) {
			return new ResponseEntity<Product>(storage.get(UUID.fromString(productID)), HttpStatus.OK);
		}
		logger.error("could not find product with id " + productID);
		return new ResponseEntity<Product>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(value = "/{productID}", method = RequestMethod.GET, produces = "application/productVersion2+json")
	public ResponseEntity<ProductV2> getVersion2(@PathVariable String productID) {
		logger.debug("Trying to find product V2 with id " + productID);
		
		if (storage.containsKey(UUID.fromString(productID))) {
			Product p =storage.get(UUID.fromString(productID));
			ProductV2 p2 = new ProductV2(p.getProductID(), p.getProductName(), p.getPrice());
			p2.add(p.getLinks());
			EnviromentInfo env = new EnviromentInfo();
			env.setDesc("Short desc for enviroment");
			env.setSustainsWater(false);
			p2.setEnviromentinfo(env);
			return new ResponseEntity<ProductV2>(p2, HttpStatus.OK);
		}
		logger.error("could not find product with id " + productID);
		return new ResponseEntity<ProductV2>(HttpStatus.NOT_FOUND);
	}
	@RequestMapping(value = "/{productID}.v2", method = RequestMethod.GET)
	public ResponseEntity<ProductV2> getVersion2_1(@PathVariable String productID) {
		logger.debug("Trying to find product V2 with id " + productID);
		
		if (storage.containsKey(UUID.fromString(productID))) {
			Product p =storage.get(UUID.fromString(productID));
			ProductV2 p2 = new ProductV2(p.getProductID(), p.getProductName(), p.getPrice());
			EnviromentInfo env = new EnviromentInfo();
			env.setDesc("Short desc for enviroment");
			env.setSustainsWater(false);
			p2.setEnviromentinfo(env);
			return new ResponseEntity<ProductV2>(p2, HttpStatus.OK);
		}
		logger.error("could not find product with id " + productID);
		return new ResponseEntity<ProductV2>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Product>> findAll() {
		List<Product> list = new ArrayList<Product>(storage.values());
		return new ResponseEntity<List<Product>>(list, HttpStatus.OK);

	}

}
