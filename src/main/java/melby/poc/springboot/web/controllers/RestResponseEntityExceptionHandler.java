package melby.poc.springboot.web.controllers;

import java.util.List;

import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import melby.poc.springboot.web.exceptions.ApiError;
import melby.poc.springboot.web.exceptions.MyInputValidationException;
import melby.poc.springboot.web.exceptions.ValidationErrorDTO;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	private Logger log = LoggerFactory.getLogger(getClass());

	public RestResponseEntityExceptionHandler() {
		super();
	}

	@Override
	protected final ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex,
			final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
		log.info("Bad Request: {}", ex.getMessage());
		log.debug("Bad Request: ", ex);

		final BindingResult result = ex.getBindingResult();
		final List<FieldError> fieldErrors = result.getFieldErrors();
		final ValidationErrorDTO dto = processFieldErrors(fieldErrors);

		return handleExceptionInternal(ex, dto, headers, HttpStatus.BAD_REQUEST, request);
	}

	@Override
	protected final ResponseEntity<Object> handleHttpMessageNotReadable(final HttpMessageNotReadableException ex,
			final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
		log.info("Bad Request: {}", ex.getMessage());
		log.debug("Bad Request: ", ex);

		final ApiError apiError = message(HttpStatus.BAD_REQUEST, ex);
		return handleExceptionInternal(ex, apiError, headers, HttpStatus.BAD_REQUEST, request);
	}

	@ExceptionHandler(value = { ConstraintViolationException.class, MyInputValidationException.class,
			IllegalArgumentException.class })
	public final ResponseEntity<Object> handleBadRequest(final RuntimeException ex, final WebRequest request) {
		log.info("Bad Request: {}", ex.getLocalizedMessage());
		log.debug("Bad Request: ", ex);

		final ApiError apiError = message(HttpStatus.BAD_REQUEST, ex);
		return handleExceptionInternal(ex, apiError, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
	}

	// @ExceptionHandler({ JsonMappingException.class })
	// public ResponseEntity<Object> handleValidationErrors(final
	// JsonMappingException ex, final WebRequest request) {
	// return handleExceptionInternal(ex, ex.getStackTrace(), new HttpHeaders(),
	// HttpStatus.BAD_REQUEST, request);
	// }

	private ApiError message(final HttpStatus httpStatus, final Exception ex) {
		final String message = ex.getMessage() == null ? ex.getClass().getSimpleName() : ex.getMessage();
		final String devMessage = ex.getClass().getSimpleName();
		// devMessage = ExceptionUtils.getStackTrace(ex);

		return new ApiError(httpStatus.value(), message, devMessage);
	}

	// UTIL

	private ValidationErrorDTO processFieldErrors(final List<FieldError> fieldErrors) {
		final ValidationErrorDTO dto = new ValidationErrorDTO();

		for (final FieldError fieldError : fieldErrors) {
			final String localizedErrorMessage = fieldError.getDefaultMessage();
			dto.addFieldError(fieldError.getField(), localizedErrorMessage);
		}

		return dto;
	}

}
