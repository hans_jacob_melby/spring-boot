package melby.poc.springboot.web.domain;
import java.util.UUID;

import javax.validation.constraints.NotNull;

import org.springframework.hateoas.ResourceSupport;


public class Product extends ResourceSupport {
	private UUID productID;
	@NotNull
	private String productName;
	private double price;
	public void setProductID(UUID productID) {
		this.productID = productID;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public Product(UUID productID, String productName, double price) {
		super();
		this.productID = productID;
		this.productName = productName;
		this.price = price;
	}
	public UUID getProductID() {
		return productID;
	}
	public String getProductName() {
		return productName;
	}
	public double getPrice() {
		return price;
	}
	public Product()
	{
		
	}
	
	
}
