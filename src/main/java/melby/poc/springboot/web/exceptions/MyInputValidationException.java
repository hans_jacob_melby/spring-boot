package melby.poc.springboot.web.exceptions;

import java.util.List;

public final class MyInputValidationException extends RuntimeException {
	private static final long serialVersionUID = -1239601612523311761L;
	private List<String> validationErrors;
		
	public MyInputValidationException(List<String> messages) {
		validationErrors=messages;
	}	
public List<String> getValidationErrors(){
	return validationErrors;
}
	

	
}
