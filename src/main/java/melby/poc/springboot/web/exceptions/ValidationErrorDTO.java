package melby.poc.springboot.web.exceptions;

import java.util.ArrayList;
import java.util.List;

import melby.poc.springboot.web.controllers.FieldErrorDTO;

public class ValidationErrorDTO {

    private final List<FieldErrorDTO> fieldErrors = new ArrayList<FieldErrorDTO>();

    public ValidationErrorDTO() {
        super();
    }

    //

    public final void addFieldError(final String path, final String message) {
        final FieldErrorDTO error = new FieldErrorDTO(path, message);
        fieldErrors.add(error);
    }

    public final List<FieldErrorDTO> getFieldErrors() {
        return fieldErrors;
    }

    //

    @Override
    public final String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("ValidationErrorDTO [fieldErrors=").append(fieldErrors).append("]");
        return builder.toString();
    }

}