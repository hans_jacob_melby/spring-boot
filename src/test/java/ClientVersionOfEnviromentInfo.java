

public class ClientVersionOfEnviromentInfo {

	private String desc;
	private boolean sustainsWater;
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public boolean isSustainsWater() {
		return sustainsWater;
	}
	public void setSustainsWater(boolean sustainsWater) {
		this.sustainsWater = sustainsWater;
	}
	
}
