


import java.util.UUID;

import org.springframework.hateoas.ResourceSupport;


public class ClientVersionOfProductV2 extends ResourceSupport {
	private UUID productID;
	private String productName;
	private double price;
	private ClientVersionOfEnviromentInfo enviromentinfo;
	public void setProductID(UUID productID) {
		this.productID = productID;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public ClientVersionOfProductV2(UUID productID, String productName, double price) {
		super();
		this.productID = productID;
		this.productName = productName;
		this.price = price;
	}
	public UUID getProductID() {
		return productID;
	}
	public String getProductName() {
		return productName;
	}
	public double getPrice() {
		return price;
	}
	public ClientVersionOfProductV2()
	{
		
	}
	public ClientVersionOfEnviromentInfo getEnviromentinfo() {
		return enviromentinfo;
	}
	public void setEnviromentinfo(ClientVersionOfEnviromentInfo enviromentinfo) {
		this.enviromentinfo = enviromentinfo;
	}
	
	
}
