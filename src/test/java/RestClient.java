
import java.util.Arrays;
import java.util.List;
import java.util.UUID;



import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;


public class RestClient {

	private RestTemplate restTemplate = new RestTemplate();
	private String url = "http://localhost:8080/products";
	public static void main (String args[]){
		
		RestClient client = new RestClient();
		client.postSampleData();
//		List<ClientVersionOfProduct> sampleData = client.getSampleData();
//		client.getUsingExhangeAndAcceptHeader(UUID.randomUUID(),MediaType.APPLICATION_JSON);
//		System.out.println("Number of elements returned : "+sampleData.size());
//		double average = sampleData
//			    .stream()
//			    //.filter(p -> p.getProductName().equalsIgnoreCase("test"))
//			    .mapToDouble(p -> p.getPrice())
//			    .average()
//			    .getAsDouble();
//		System.out.println("avg pris på produkter er : "+average);
		//UUID  existingUUID = sampleData.stream().findAny().get().getProductID();
//		UUID existingUUID = UUID.fromString("40c9f12f-66c9-4f50-a524-2072ea68c2d1");
//		client.getUsingExhangeAndAcceptHeaderForVersion2(existingUUID,MediaType.valueOf("application/productVersion2+json"));
		
		
		
		 
		 
		
	}
	public void postSampleData(){
		
		ClientVersionOfProduct product = new ClientVersionOfProduct();
		product.setPrice(1000D);
		product.setProductName("TestProduct2");
		
		createUsingPostForEntity(product);		
		createusingExchange(product);
		
		
		
	}
	
	private void getUsingExhangeAndAcceptHeader(UUID productId, MediaType mediaType) {
		try{
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(mediaType));
		
		HttpEntity<ClientVersionOfProduct> entity = new HttpEntity<ClientVersionOfProduct>(null, headers);
		
		ResponseEntity<ClientVersionOfProduct> result = restTemplate.exchange(url+"/"+productId.toString(), HttpMethod.GET, entity, ClientVersionOfProduct.class);
		
		
		System.out.println(result.getStatusCode()+" " +result.getStatusCode().getReasonPhrase());
		ClientVersionOfProduct product = result.getBody();
		System.out.println(product.getProductName());
		
		}
		catch (HttpClientErrorException e){
			System.out.println(e.getMessage()+" "+productId);
		}
		
		
		
	}
	
	
	private void getUsingExhangeAndAcceptHeaderForVersion2(UUID productId, MediaType mediaType) {
		try{
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(mediaType));
		
		HttpEntity<ClientVersionOfProductV2> entity = new HttpEntity<ClientVersionOfProductV2>(null, headers);
		
		ResponseEntity<ClientVersionOfProductV2> result = restTemplate.exchange(url+"/"+productId.toString(), HttpMethod.GET, entity, ClientVersionOfProductV2.class);
		
		
		
		System.out.println(result.getStatusCode()+" " +result.getStatusCode().getReasonPhrase());
		ClientVersionOfProductV2 product = result.getBody();
		System.out.println(product.getProductName());
		System.out.println(product.getEnviromentinfo().getDesc());
		System.out.println(product.getLinks().size());
		
		}
		catch (HttpClientErrorException e){
			System.out.println(e.getMessage()+" "+productId);
		}
		
		
		
	}
	private void createusingExchangeAndSetheaders(ClientVersionOfProduct product) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<ClientVersionOfProduct> entity = new HttpEntity<ClientVersionOfProduct>(product, headers);
		ResponseEntity<ClientVersionOfProduct> exchange = restTemplate.exchange(url, HttpMethod.POST, entity, ClientVersionOfProduct.class);
		System.out.println(exchange.getStatusCode());
	}
	private void createusingExchange(ClientVersionOfProduct product) {
		MediaType contentMediaType=MediaType.valueOf("application/productVersion2+json");
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(contentMediaType));
		HttpEntity<ClientVersionOfProduct> entity = new HttpEntity<ClientVersionOfProduct>(product, headers);
		ResponseEntity<ClientVersionOfProduct> exchange = restTemplate.exchange(url, HttpMethod.POST, entity, ClientVersionOfProduct.class);
		System.out.println(exchange.getStatusCode());
	}
	
	private void createUsingPostForEntity(ClientVersionOfProduct product) {
		ResponseEntity<ClientVersionOfProduct> result = restTemplate.postForEntity(url, product, ClientVersionOfProduct.class);
		System.out.println(result.getStatusCode());
		
	}
	private void createusingpostForObject(ClientVersionOfProduct product) {
		ClientVersionOfProduct result2 = restTemplate.postForObject( url, product, ClientVersionOfProduct.class);
		System.out.println(result2.getProductID());
	}
	
	public List<ClientVersionOfProduct> getSampleData(){
		ResponseEntity<ClientVersionOfProduct[]> result = restTemplate.exchange(url, HttpMethod.GET, null, ClientVersionOfProduct[].class);
		System.out.println(result.getStatusCode()+" " +result.getStatusCode().getReasonPhrase());
		 
		 ClientVersionOfProduct[] body = result.getBody();
		 List<ClientVersionOfProduct> list = Arrays.asList(body);
		return list;
		
	}
	
	
}
